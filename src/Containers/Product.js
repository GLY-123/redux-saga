import {connect} from "react-redux" //将该组件变成容器组件
import Product from "../component/Product"
const mapState=state=>{
    return { //可以拿到store中的state
        state
    }
}
const mapDispatch=dispatch=>{
    return {//可以拿到store中的disparch
       log(){
            console.log('111');
        }
    }
}

export default connect (mapState,mapDispatch)(Product)

//Product,和Cart为App下面的子组件，即可以拿到store中的state和dispatch,
//然后用connect将store中的state和dispatch注入到Product中,此时，组件product就会得到store中的数据