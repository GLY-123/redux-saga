
import React from 'react'
 import Product from "./Containers/Product"   
 import Cart from "./Containers/Cart"                                      
class App extends React.Component{
    render(){
        return(
            <fieldset>
                <legend>App</legend>
                <Product/>
                <hr/>
                <Cart/>
            </fieldset>
        )
    }
}
export default App